#!/usr/bin/node
config = require("./config.json");
fs = require('fs');
git = require('simple-git');
var replaceAll = require("replaceall");
const repo = git(config.repoPath);
repo
    .silent(true)
    .pull([], function(){
        var fileBuffer = fs.readFileSync(config.basePath);
        ircdConfig = fileBuffer.toString();
        ircdConfig = replaceAll("&ipv4-addr;", config.ipv4, ircdConfig);
        ircdConfig = replaceAll("&ipv6-addr;", config.ipv6, ircdConfig);
        ircdConfig = replaceAll("&network-name;", config.networkName, ircdConfig);
        ircdConfig = replaceAll("&server;", config.serverName, ircdConfig);
        ircdConfig = replaceAll("&adminName;", config.adminName, ircdConfig);
        ircdConfig = replaceAll("&adminNick;", config.adminNick, ircdConfig);
        ircdConfig = replaceAll("&adminEmail;", config.adminEmail, ircdConfig);
        /* END OF REPLACEMENTS */
        console.log('# Finished pulling repository at ' + config.repoPath);
        console.log(ircdConfig);
    });
