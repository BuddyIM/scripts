#!/bin/sh
. /etc/rc.subr
name=ircd
rcvar=ircd_enable

start_cmd="${name}_start"
stop_cmd="${name}_stop"

load_rc_config $name2
: ${ircd_enable:=no} 3
: ${ircd_msg="Nothing started."}4

ircd_start()
{
	su - irc -c '/opt/inspircd/bin/inspircd'
}

ircd_stop()
{
    kill -9 $(cat /opt/inspircd/data/inspircd.pid)
}

run_rc_command "$1"