This repository is just a collection of scripts made public by Buddy.IM. This repository is not complete and will be added to as more scripts are made public.

## config-manager
config-manager is a script which pulls remote configuration files for InspIRCd via git. config-manager always returns a configuration file whether old or new, so there is no risk of accidentally unloading modules and oper blocks for example.

## ircd.service
This is just a systemd script for controlling your IRCd. It's very basic but it will get the job done.